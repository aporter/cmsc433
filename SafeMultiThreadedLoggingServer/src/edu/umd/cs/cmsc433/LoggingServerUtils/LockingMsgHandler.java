package edu.umd.cs.cmsc433.LoggingServerUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


public class LockingMsgHandler implements Runnable {

	private static final Map<String, Object> locks = new HashMap<String, Object>();
	private DataRecord record; 
	protected PrintWriter out;
	
	public LockingMsgHandler(DataRecord record) {
		this.record = record;
	}

	private static synchronized Object getLock(String id) {
		if (!locks.containsKey(id)) {
			locks.put(id, new Object());
		}
		return locks.get(id);
	}
	
	public void addRecord (DataRecord record) {
		this.record = record;
	}

	public void run() {

		System.out.println("Server Side: writing record:" + record);
		writeMsg();
	}

	protected void writeMsg() {
		try {
			String sender = record.getSender();
			String msg = record.getMsg();

			out = new PrintWriter(new FileWriter(record.getSender() + ".txt",
					true));

			synchronized (getLock(sender)) {
				out.print("Begin Record:");
				out.flush();
				out.print("Sender:" +sender + ":");
				out.flush();
				out.print("Message:" + msg + ":");
				out.flush();
				out.println("End Record");
				out.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}
	}
}