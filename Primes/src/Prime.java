import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Prime implements Runnable {
	private static int N; /* NEW -- N now immutable, start a local variable */
	private static int numThreads;
	private static List<Integer> primes;
	private static List<Boolean> notPrime;
	private static int total = 0;

	private static boolean is_prime(int v) {
		int i;
		int bound = (int) Math.floor(Math.sqrt((double) v)) + 1;
		for (i = 2; i < bound; i++) {
			/* NEW -- sFixed data race here */
			synchronized (notPrime) {
				if (notPrime.get(i))
					continue;
				if (v % i == 0) {
					notPrime.set(v, true);
					return false;
				}
			}
		}
		return (v > 1);
	}

	private int threadNum;

	private Prime(int threadNum) {
		this.threadNum = threadNum;
	}

	public void run() {
		int i;
		int start = N / numThreads * threadNum;
		/* NEW: use local variable end to hold last thread index */  
		int end = start + N / numThreads;
		for (i = start; i < end; i++) {
			if (is_prime(i)) {
				/* NEW:Fix data race on primes and total */
				synchronized (primes) {
					primes.add(i);
					total++;
				}
			}
		}
	}

	public static void main(String args[]) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Args: numPrimes numThreads");
			System.exit(1);
		}

		N = new Integer(args[0]).intValue();
		primes = new ArrayList<Integer>();
		notPrime = new ArrayList<Boolean>(N);
		for (int i = 0; i < N; i++) {
			notPrime.add(false);
		}

		numThreads = new Integer(args[1]).intValue();
		Thread[] threads = new Thread[numThreads - 1];

		for (int i = 0; i < numThreads; i++) {
			if (i != numThreads - 1) {
				threads[i] = new Thread(new Prime(i));
				threads[i].start();
			} else {
				new Prime(i).run();
			}
		}
		/* NEW:must wait for threads to finish */
		for (int i = 0; i < numThreads - 1; i++) {
			threads[i].join();
		}

		System.out.println("Number of prime numbers between 2 and " + N + ": "
				+ total);
		Collections.sort(primes);
		System.out.println(primes);
	}
}