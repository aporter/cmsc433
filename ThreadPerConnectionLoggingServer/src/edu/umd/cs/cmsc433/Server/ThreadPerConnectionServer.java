package edu.umd.cs.cmsc433.Server;

import edu.umd.cs.cmsc433.LoggingServerUtils.DataRecord;
import edu.umd.cs.cmsc433.LoggingServerUtils.StdOutMsgHandler;

public class ThreadPerConnectionServer extends LoggingServerCore {

	ThreadPerConnectionServer(int port) {
		super(port);
	}


	public void process(DataRecord record) {
		(new Thread(new StdOutMsgHandler (record))).start();
	}
	
	
	public static void main(String[] args) {
		System.out.println("Starting server on port:" + PORT);
		ThreadPerConnectionServer s = new ThreadPerConnectionServer(PORT);
		s.go();
		System.out.println("Shutting down server on port:" + PORT);
	}
}