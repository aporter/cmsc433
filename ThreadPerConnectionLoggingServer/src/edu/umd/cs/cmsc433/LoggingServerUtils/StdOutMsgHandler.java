package edu.umd.cs.cmsc433.LoggingServerUtils;

public class StdOutMsgHandler extends MsgHandler {

	public StdOutMsgHandler(DataRecord record) {
		super(record);
	}

	public void run() {
		System.out.println("Server Side: writing record:" + record);
	}
}
