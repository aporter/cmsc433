import java.util.Random;

/**
 * We have broken up the computation of the average into computation of chunks
 * of the intial sum. Here, they happen one at a time, but in the final version
 * they will happen in parallel.
 */
public class ArrayAverageChunk {

	/**
	 * Chunked unit of work for computing the sum.
	 */
	private static double sum(int start, int end, Double arr[]) {
		double tmp = 0;
		for (int i = start; i < end; i++) {
			tmp += arr[i];
		}
		return tmp;
	}

	private static int num = 2500000;
	private static int units = 2;
	private static Double arr[];

	/**
	 * Single-threaded, chunked computation
	 */
	private static void computeSTchunk() {
		long start = System.currentTimeMillis();
		double tmp = 0;
		for (int i = 0; i < units; i++) {
			int startIdx = i * (num / units);
			int endIdx = (i == units - 1) ? num : (i + 1) * (num / units);
			tmp += sum(startIdx, endIdx, arr);
		}
		tmp = tmp / (double) num;
		long end = System.currentTimeMillis();
		System.out.println("Result (ST, " + (end - start) + " ms) = " + tmp);
	}

	/**
	 * Creates an array of the given size with random contents.
	 */
	static private Random rand = new Random();

	public static Double[] createRand(int num) {
		Double[] result = new Double[num];
		for (int i = 0; i < num; i++)
			result[i] = rand.nextDouble();
		return result;
	}

	public static void main(String args[]) {
		if (args.length >= 1)
			num = new Integer(args[0]);
		if (args.length >= 2)
			units = new Integer(args[1]);
		System.out.println("array size = " + num);
		System.out.println("units = " + units);
		System.out.println("Constructing input array ...");
		System.out.flush();

		arr = createRand(num);

		System.out.println("Starting runs ...");
		System.out.flush();
		computeSTchunk();
	}
}