import java.util.Random;

/** Initial implementation.  This simply creates an array and computes
 * its average. */
public class ArrayAverage {
    private static int num = 2500000;
    private static Double arr[];

    /**
     * Single-threaded computation
     */
    private static void computeST() {
        long start = System.currentTimeMillis();
        double tmp = 0;
        for (int i = 0; i<num; i++) {
            tmp += arr[i];
        }
        tmp = tmp / (double)num;
        long end = System.currentTimeMillis();
        System.out.println("Result (ST, "+(end-start)+" ms) = "+tmp);
    }

    /** 
     * Creates an array of the given size with random contents.
     */
    static private Random rand = new Random();
    
    public static Double[] createRand(int num) {
        Double[] result = new Double[num];
        for(int i = 0 ; i < num; i++)
            result[i] = rand.nextDouble();
        return result;
    }

    public static void main(String args[]) {
        if (args.length >= 1)
            num = new Integer(args[0]);
        System.out.println("array size = "+num);
        System.out.println("Constructing input array ...");
        System.out.flush();

        arr = createRand(num);

        System.out.println("Starting runs ...");
        System.out.flush();
        computeST();
    }    
}