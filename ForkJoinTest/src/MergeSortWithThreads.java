import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MergeSortWithThreads implements Runnable {

	final int[] numbers;
	final int startPos, endPos;
	final int[] result;

	final int SEQUENTIAL_THRESHOLD = 10;

	public MergeSortWithThreads(int[] numbers, int startPos, int endPos) {
		super();
		this.numbers = numbers;
		this.startPos = startPos;
		this.endPos = endPos;
		result = new int[numbers.length];
	}

	private void merge(MergeSortWithThreads left, MergeSortWithThreads right) {
		int i = 0, leftPos = 0, rightPos = 0, leftSize = left.size(), rightSize = right
				.size();
		while (leftPos < leftSize && rightPos < rightSize)
			result[i++] = (left.result[leftPos] <= right.result[rightPos]) ? left.result[leftPos++]
					: right.result[rightPos++];
		while (leftPos < leftSize)
			result[i++] = left.result[leftPos++];
		while (rightPos < rightSize)
			result[i++] = right.result[rightPos++];
	}

	public int size() {
		return endPos - startPos;
	}

	public void run () {
		if (size() < SEQUENTIAL_THRESHOLD) {
			System.arraycopy(numbers, startPos, result, 0, size());
			Arrays.sort(result, 0, size());
		} else {
			int midpoint = size() / 2;
			MergeSortWithThreads left = new MergeSortWithThreads(numbers, startPos, startPos
					+ midpoint);
			MergeSortWithThreads right = new MergeSortWithThreads(numbers, startPos + midpoint,
					endPos);
			// invokeAll(left, right);
			Thread t1 = new Thread(left);
			Thread t2 = new Thread(right);
			t1.start();
			t2.start();
			try {
				t1.join();
				t2.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			merge(left, right);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		int len = 10000;
		int[] nums = new int[len];
		Random r = new Random();

		for (int i = 0; i < len; i++) {
			nums[i] = r.nextInt(len);
		}

		// ForkJoinPool pool = new ForkJoinPool();
		ExecutorService exec = Executors.newCachedThreadPool();
		MergeSortWithThreads whole = new MergeSortWithThreads(nums, 0, nums.length);
		long startTime = System.currentTimeMillis(); 
		exec.submit(whole);
		exec.shutdown();
		exec.awaitTermination(1000, TimeUnit.SECONDS);
		long endTime = System.currentTimeMillis();
		System.out.println("Time:" + (endTime-startTime));
		System.out.println(Arrays.toString(whole.result));
	}
}