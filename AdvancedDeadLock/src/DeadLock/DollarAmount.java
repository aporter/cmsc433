package DeadLock;

class DollarAmount implements Comparable<DollarAmount> {
    int amount;
    
public DollarAmount(int amount) {
	this.amount = amount;
}

public DollarAmount add(DollarAmount d) {
    return new DollarAmount(this.amount + d.amount);
}

public DollarAmount subtract(DollarAmount d) {
    return new DollarAmount(this.amount - d.amount);
}

public int compareTo(DollarAmount dollarAmount) {
    return this.amount < dollarAmount.amount ? -1 : (this.amount > dollarAmount.amount ? 1 : 0);
}
}
