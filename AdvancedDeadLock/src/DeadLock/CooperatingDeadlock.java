package DeadLock;

import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

/**
 * CooperatingDeadlock
 * <p/>
 * Lock-ordering deadlock between cooperating objects
 * 
 * @author Brian Goetz and Tim Peierls
 */

/*
 * Assume each Taxi is simulated in its own Thread
 * Assume the Dispatcher in its own Thread and periodically updates a map showing all the Taxis
 * Taxi Thread T1 holds lock on taxi1 during setLocation(). In setLocation() calls Dispatcher.notifyAvailable()
 * Dispatcher holds lock on self in updateMap() and calls taxi1.getLocation()
 */

public class CooperatingDeadlock {
	// Warning: deadlock-prone!
	class Taxi {
		private Point location, destination;
		private final Dispatcher dispatcher;

		public Taxi(Dispatcher dispatcher) {
			this.dispatcher = dispatcher;
		}

		public synchronized Point getLocation() {
			return location;
		}

		public synchronized void setLocation(Point location) {
			this.location = location;
			if (location.equals(destination))
				// alien call with lock held
				dispatcher.notifyAvailable(this);
		}

		public synchronized Point getDestination() {
			return destination;
		}

		public synchronized void setDestination(Point destination) {
			this.destination = destination;
		}
	}

	class Dispatcher {
		private final Set<Taxi> taxis;
		private final Set<Taxi> availableTaxis;

		public Dispatcher() {
			taxis = new HashSet<Taxi>();
			availableTaxis = new HashSet<Taxi>();
		}

		public synchronized void notifyAvailable(Taxi taxi) {
			availableTaxis.add(taxi);
		}

		public synchronized Image updateMap() {
			Image image = new Image();
			for (Taxi t : taxis)
				// alien call with lock held
				image.drawMarker(t.getLocation());
			return image;
		}
	}

	class Image {
		public void drawMarker(Point p) {
		}
	}
}
