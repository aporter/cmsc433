package DeadLock;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Account {
	private DollarAmount balance;
	private final int acctNo;
	private static final AtomicInteger sequence = new AtomicInteger();

	public Account() {
		acctNo = sequence.incrementAndGet();
		this.balance = new DollarAmount(new Random().nextInt(10000));
	}

	void debit(DollarAmount d) {
		balance = balance.subtract(d);
	}

	void credit(DollarAmount d) {
		balance = balance.add(d);
	}

	DollarAmount getBalance() {
		return balance;
	}

	int getAcctNo() {
		return acctNo;
	}
}
