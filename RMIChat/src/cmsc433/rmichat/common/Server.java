package cmsc433.rmichat.common;

/** The server controls a chat room and allows clients to log-on,
    giving them Connections when they do so
	*/
public interface Server extends java.rmi.Remote {
	/** Invoked by client to log in to chat room
	 * @param name Name client wishes to be identified by
	 * @param c Client object for callbacks
	 * @return Connection client should use for sending messages
	 * to chat room
	 */
	public Connection logon(String name, Client c) throws java.rmi.RemoteException;
}
