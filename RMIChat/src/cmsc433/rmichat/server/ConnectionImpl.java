package cmsc433.rmichat.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import cmsc433.rmichat.common.Client;
import cmsc433.rmichat.common.Connection;

/** An implementation of a connection */
public class ConnectionImpl extends UnicastRemoteObject implements Connection {

	private static final long serialVersionUID = 1L;

	// Name of user that owns this connection
	private String name;

	// Map from use names to their connections
	private Map<String, Client> connections;

	// The client the user is attached to
	private Client client;

	// The server running the show
	private ServerImpl server;

	public ConnectionImpl(String nm, Client c, ServerImpl s,
			Map<String, Client> conn) throws RemoteException {
		name = nm;
		client = c;
		connections = conn;
		server = s;
		System.out.println(name + " logged on");
	};

	public void say(String msg) throws RemoteException {
		HashMap<String, Client> sendTo;
		System.out.println(name + ": " + msg);
		// In case hashtable changes while sending out messages,
		// clone it
		synchronized (connections) {
			sendTo = new HashMap<String, Client> (connections);
		}
		for (String who : sendTo.keySet()) {
			Client c = sendTo.get(who);
			try {
				c.wasSaid(name, msg);
			} catch (RemoteException err) {
				System.out.println("Dropping " + who + " due to remote error");
				synchronized (connections) {
					connections.remove(who);
				}
			}
		}
	}

	public void say(String who, String msg) throws RemoteException {
		Client c;
		synchronized (connections) {
			c = connections.get(who);
		}
		;
		msg = "[ " + msg + " ]";
		try {
			c.wasSaid(name, msg);
		} catch (RemoteException err) {
			System.out.println("Dropping " + who + " due to remote error");
			synchronized (connections) {
				connections.remove(who);
			}
		}
		client.wasSaid(name, msg);
	}

	public String[] users() throws RemoteException {
		//int sz;
		String[] w;
		synchronized (connections) {
			//sz = connections.size();
			w = connections.keySet().toArray(new String[0]);
			// w = new String[sz];
			//int i = 0;
			// for (Enumeration<String> e = connections.keys(); e
			//		.hasMoreElements(); i++)
			//	w[i] = e.nextElement();
		}
		return w;
	}

	public void logoff() throws RemoteException {
		synchronized (connections) {
			connections.remove(name);
		}
		client.wasSaid("Server", "good bye");
		server.notifyWhoChanged();
		System.out.println(name + " logged off");
	}

}
