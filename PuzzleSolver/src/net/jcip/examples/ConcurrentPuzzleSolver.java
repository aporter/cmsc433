package net.jcip.examples;

import java.util.*;
import java.util.concurrent.*;

public class ConcurrentPuzzleSolver <POS_REP, MOVE_REP> {
    private final IPuzzle<POS_REP, MOVE_REP> puzzle;
    private final ExecutorService exec;
    private final ConcurrentMap<POS_REP, Boolean> seen;
    protected final ValueLatch<PuzzleNode<POS_REP, MOVE_REP>> solution = new ValueLatch<PuzzleNode<POS_REP, MOVE_REP>>();

    public ConcurrentPuzzleSolver(IPuzzle<POS_REP, MOVE_REP> puzzle) {
        this.puzzle = puzzle;
        this.exec = initThreadPool();
        this.seen = new ConcurrentHashMap<POS_REP, Boolean>();
        if (exec instanceof ThreadPoolExecutor) {
            ThreadPoolExecutor tpe = (ThreadPoolExecutor) exec;
            tpe.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        }
    }

    private ExecutorService initThreadPool() {
        return Executors.newCachedThreadPool();
    }

    /** 
     * 
     * @return The list of nodes on path from initial position to goal
     * @throws InterruptedException
     */
    public List<MOVE_REP> solve() throws InterruptedException {
        try {
            POS_REP p = puzzle.initialPosition();
            exec.execute(newTask(p, null, null));

            // block until solution found
            PuzzleNode<POS_REP, MOVE_REP> solnPuzzleNode = solution.getValue();
            
			// reconstruct solution backwards from goal position
            return (solnPuzzleNode == null) ? null : solnPuzzleNode.asMoveList();
        } finally {
            exec.shutdown();
        }
    }

    protected Runnable newTask(POS_REP p, MOVE_REP m, PuzzleNode<POS_REP, MOVE_REP> n) {
        return new SolverTask(p, m, n);
    }

    protected class SolverTask extends PuzzleNode<POS_REP, MOVE_REP> implements Runnable {

    	SolverTask(POS_REP pos, MOVE_REP move, PuzzleNode<POS_REP, MOVE_REP> prev) {
            super(pos, move, prev);
        }

        public void run() {
        	
        	// Have we already seen this position?
        	if (solution.isSet()
                    || seen.putIfAbsent(pos, true) != null)
                return; // already solved or seen this position
        	
        	System.out.println("evaluting position:" + pos);
        	
        	// Are we at the goal position?
        	if (puzzle.isGoal(pos)) {
        		// set value in result-bearing latch
                solution.setValue(this);
				System.out.println("Solution Found:" + pos);
            }
            else {
    			// Not at the goal position. Search forward over all possible moves 
                for (MOVE_REP m : puzzle.legalMoves(pos)) {
                    exec.execute(newTask(puzzle.move(pos, m), m, this));
                }
            }
        }
    }
}
