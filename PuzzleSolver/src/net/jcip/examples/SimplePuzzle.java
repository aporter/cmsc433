package net.jcip.examples;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class SimplePuzzle implements IPuzzle<Integer, Move1DEnum> {
	private final static int DIM = 10; 
	private final static int mGoal = new Random().nextInt(DIM); 
	private final static int mInitialPos = new Random().nextInt(DIM); 
	
	@Override
	public Integer initialPosition() {
		return mInitialPos;
	}

	@Override
	public boolean isGoal(Integer position) {
		return position == mGoal;
	}

	@Override
	public Set<Move1DEnum> legalMoves(Integer position) {
		Set<Move1DEnum> tmp = new HashSet<Move1DEnum>();
		if (position > 0) tmp.add(Move1DEnum.RIGHT);
		tmp.add(Move1DEnum.LEFT);
		return tmp;
	}

	@Override
	public Integer move(Integer position, Move1DEnum move) {
		if (move==Move1DEnum.RIGHT) return position - 1;
		else return position + 1;
	}

}
