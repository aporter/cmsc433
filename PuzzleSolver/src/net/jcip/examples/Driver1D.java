package net.jcip.examples;

public class Driver1D {
	public static void main(String[] args) throws InterruptedException {
		SimplePuzzle sp = new SimplePuzzle();
		SequentialPuzzleSolver<Integer, Move1DEnum> solver = new SequentialPuzzleSolver<Integer, Move1DEnum>(sp);
		// ConcurrentPuzzleSolver<Integer, Move1DEnum> solver = new ConcurrentPuzzleSolver<Integer, Move1DEnum>(sp);
		// PuzzleSolver<Integer, Move1DEnum> solver = new PuzzleSolver<Integer, Move1DEnum>(sp);
		
		System.out.println(solver.solve());
	}
}
