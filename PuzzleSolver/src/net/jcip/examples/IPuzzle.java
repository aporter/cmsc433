package net.jcip.examples;

import java.util.*;

public interface IPuzzle <POS_REP, MOVE_REP> {

	POS_REP initialPosition();

    boolean isGoal(POS_REP position);

    Set<MOVE_REP> legalMoves(POS_REP position);

    POS_REP move(POS_REP position, MOVE_REP move);
}
