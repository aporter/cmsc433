package net.jcip.examples;

import java.util.LinkedList;
import java.util.List;

public class PuzzleNode <POS_REP, MOVE_REP> {
    final POS_REP pos;
    final MOVE_REP move;
    final PuzzleNode<POS_REP, MOVE_REP> prev;

    public PuzzleNode(POS_REP pos, MOVE_REP move, PuzzleNode<POS_REP, MOVE_REP> prev) {
        this.pos = pos;
        this.move = move;
        this.prev = prev;
    }

    List<MOVE_REP> asMoveList() {
        List<MOVE_REP> solution = new LinkedList<MOVE_REP>();
        for (PuzzleNode<POS_REP, MOVE_REP> n = this; n.move != null; n = n.prev)
            solution.add(0, n.move);
        return solution;
    }
}
