package edu.umd.cs.cmsc433.ConcurrentCollections;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class SemaphoreTunnel {

	// size refers to the maximum number of cars that can enter the tunnel
	private int size = 2;
	
	final static CountDownLatch start = new CountDownLatch(1);
	Random r = new Random();

	Semaphore spaces = new Semaphore(size);

	public void enter(int number) throws InterruptedException {
		spaces.acquire();
		System.out.println("Car #" + number + " enters.");

	}

	public void exit(int number) throws InterruptedException {
		System.out.println("Car #" + number + " exits");
		spaces.release();
	}

	static class Car implements Runnable {
		private SemaphoreTunnel tunnel;
		private int number;

		public Car(SemaphoreTunnel st, int number) {
			tunnel = st;
			this.number = number;
		}

		public void run() {
			try {
				start.await();
				while (!Thread.interrupted()) {
					tunnel.enter(number);
					Thread.sleep(tunnel.r.nextInt(100));
					tunnel.exit(number);
					Thread.sleep(tunnel.r.nextInt(100));

				}
			} catch (InterruptedException e1) {
			}
		}

		public static void main(String[] args) throws InterruptedException {
			SemaphoreTunnel st = new SemaphoreTunnel();
			Car c1 = new Car(st, 1);
			Car c2 = new Car(st, 2);
			Car c3 = new Car(st, 3);
			Car c4 = new Car(st, 4);
			
			Thread tc1 = new Thread(c1);
			tc1.start();
			Thread tc2 = new Thread(c2);
			tc2.start();
			Thread tc3 = new Thread(c3);
			tc3.start();
			Thread tc4 = new Thread(c4);
			tc4.start();
			
			start.countDown();
			
			Thread.sleep(500);
			
			tc1.interrupt();
			tc2.interrupt();
			tc3.interrupt();
			tc4.interrupt();
			System.out.println("Main method ending");
		}
	}
}
