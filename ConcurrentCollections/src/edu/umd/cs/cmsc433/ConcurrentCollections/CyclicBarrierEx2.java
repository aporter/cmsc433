package edu.umd.cs.cmsc433.ConcurrentCollections;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarrierEx2 {
	CyclicBarrier barrier;

	List<Result> results = new ArrayList<Result>();

	static long timeConnect(String site) {
		long start = System.currentTimeMillis();
		try {
			new URL(site).openConnection().connect();
		} catch (IOException e) {
			return -1;
		}
		return System.currentTimeMillis() - start;
	}

	void showResults() {
		Collections.sort(results);
		for (Result result : results)
			System.out.printf("%-30.30s : %d\n", result.site, result.time);
		System.out.println("------------------");
	}

	public void go(String[] args) throws InterruptedException {
		Runnable showResultsAction = new Runnable() {
			public void run() {
				showResults();
				results.clear();
			}
		};
		barrier = new CyclicBarrier(args.length, showResultsAction);
		ArrayList<Thread> ThreadList = new ArrayList<Thread> (args.length) ;
		// barrier action used here
		for (final String site : args) {
			 ThreadList.add(new Thread (new Runnable() {
				public void run() {
					while (!Thread.interrupted()) {
						System.out.println("Trying:" + site);						
						long time = timeConnect(site);
						results.add(new Result(time, site));
						System.out.println("time:" + time + " site:" + site);						
						try {
							barrier.await();
						} catch (BrokenBarrierException e) {
							System.out.println("broken site:" + site);
							//e.printStackTrace();
							return;
						} catch (InterruptedException e) {
							//e.printStackTrace();
						}
					}
					System.out.println("exiting");
				}
			}));
		ThreadList.get(ThreadList.size()-1).start();
		}
		Thread.sleep(5000);
		System.out.println("Got to end of thread loop");
		for (final Thread t : ThreadList) {
			if (t.isAlive()) t.interrupt();
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		new CyclicBarrierEx2().go(new String[] { 
				"http://www.yahoo.com",
				"http://www.google.com", 
				"http://www.comnap.aq/",
				"http://www.uct.ac.za/"
				});
	}
}

class Result implements Comparable<Result> {
	Long time;

	String site;

	Result(Long time, String site) {
		this.time = time;
		this.site = site;
	}

	public int compareTo(Result r) {
		return time.compareTo(r.time);
	}
}