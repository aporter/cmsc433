package edu.umd.cs.cmsc433.ConcurrentCollections;
import java.util.concurrent.*;
import java.util.concurrent.ExecutorService;

public class FutureTaskStringReverser {
	
	SlowStringReverser reverser = new SlowStringReverser();

	// Reverse target string

	void doReverse(final String target) throws InterruptedException {

		FutureTask<String> future = new FutureTask<String>(
				new Callable<String>() {
					public String call() {
						return reverser.reverseString(target);
					}
				});
		new Thread (future).start();

		// Check every 0.5 seconds
		// Instead we could do a long operation here while we
		// wait for reverser.reverseString() to finish

		while (!future.isDone()) {
			System.out.println("Task not yet completed.");
			try {
				Thread.sleep(500);
			} catch (InterruptedException ie) {
				System.out.println("Will check after 1/2 sec.");
			}
		}
		try {
			System.out.println("Here is the result..." + future.get());
		} catch (ExecutionException ex) {
		}
		return;
	}

	// reverse the string "foobar"
	
	public static void main(String args[]) {
		FutureTaskStringReverser msr = new FutureTaskStringReverser();
		try {
			msr.doReverse("foobar");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// Slow reverser takes 1 sec. per character

class SlowStringReverser {
	StringBuffer orgString;
	StringBuffer reversedString;

	public String reverseString(String str) {
		orgString = new StringBuffer(str);
		reversedString = new StringBuffer();
		for (int i = (orgString.length() - 1); i >= 0; i--) {

			reversedString.append(orgString.charAt(i));
			System.out.println("Reversing one character per second."
					+ reversedString);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {}
		}
		return reversedString.toString();
	}
}
